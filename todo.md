#assets
#refactor obstacles into proper types with 
#game board
#entities
  + skier
  + obstacles


structure

entity
  + setup
  + update
  + draw

game manager
  setup
   - call setup of all actors
  update
    - update all actors
  draw 
    - draw all actors
