# Ceros Ski Code Challenge

Welcome to the Ceros Code Challenge - Ski Edition!

For this challenge, we have included some base code for Ceros Ski, our version of the classic Windows game SkiFree. If
you've never heard of SkiFree, Google has plenty of examples. Better yet, you can play our version here:
http://ceros-ski.herokuapp.com/

We understand that everyone has varying levels of free time, so take a look through the requirements below and let us
know when you will have something for us to look at (we also get to see how well you estimate and manage your time!).
Previous candidates have taken about 8 hours to complete our challenge. We feel this gives us a clear indicator of your
technical ability and a chance for you to show us how much you give a shit (one of Ceros's core values!) about the position
you're applying for. If anything is unclear, don't hesitate to reach out.

Requirements:
* The base game that we've sent you is not what we would consider production ready code. In fact, it's pretty far from
  it. As part of our development cycle, all code must go through a review. We would like you to perform a review
  on the base code and fix/refactor it. Is the codebase maintainable, unit-testable, and scalable? What design patterns
  could we use?

  **We will be judging you based upon how you update the code & architecture.**
* There is a bug in the game. Well, at least one bug that we know of. Use the following bug report to debug the code
  and fix it.
  * Steps to Reproduce:
    1. Load the game
    1. Crash into an obstacle
    1. Press the left arrow key
  * Expected Result: The skier gets up and is facing to the left
  * Actual Result: Giant blizzard occurs causing the screen to turn completely white (or maybe the game just crashes!)
* The game's a bit boring as it is. Add a new feature to the game to make it more enjoyable. We've included some ideas for
  you below (or you can come up with your own new feature!). You don't need to do all of them, just pick something to show
  us you can solve a problem on your own.
  * Implement jumps. The asset file for jumps is already included. All you gotta do is make the guy jump. We even included
      some jump trick assets if you wanted to get really fancy!
  * Add a score. How will you know that you're the best Ceros Skier if there's no score? Maybe store that score
      somewhere so that it is persisted across browser refreshes.
  * Feed the hungry Rhino. In the original Ski Free game, if you skied for too long, a yeti would chase you
      down and eat you. In Ceros Ski, we've provided assets for a Rhino to catch the skier.
* Update this README file with your comments about your work; what was done, what wasn't, features added & known bugs.
* Provide a way for us to view the completed code and run it, either locally or through a cloud provider
* Be original. Don’t copy someone else’s game implementation!

Bonus:
* Provide a way to reset the game once the game is over
* Provide a way to pause and resume the game
* Skier should get faster as the game progresses
* Deploy the game to a server so that we can play it without setting something up ourselves. We've included a
  package.json and web.js file that will enable this to run on Heroku. Feel free to use those or use your own code to
  deploy to a cloud service if you want.
* Write unit tests for your code

And don't think you have to stop there. If you're having fun with this and have the time, feel free to add anything else
you want and show us what you can do!

We are looking forward to see what you come up with!


## Selasie's Updates

### What was done
+ Refactor of codebase into separate classes, `GameManager`, `Skier`, `Obstacle`, `ScoreBoard`, `InstructionScreen`, `GameOverScreen`, `Storage` and 
  singleton objects namely, AsssetManger, Physics,Ui-tils. 
  1. The GameManger class is responsible for the entire gameplay. It has access to the all entities and screens in the game;
  2. Each class implements three methods `setup`, `draw` and `update`. This game manager uses calls these methods in its own `setup`, `draw` and `update` methods; 
     This allows us to maintain a contract though not strictly enforced. In the event that we would want to scale this codebase in the future by using inheritance or
     a `component entity system design(CES)` or a strategy design pattern (for opponent models such as the rhino or other skiers), we can easily refactor the 
     screens and entities without much effort without since we already maintain a contract.
  3. Moving magic strings and numbers into well defined constants. eg. `GameKeys`,  `GameStates`,`SkierState`.

+ Fixed bug which occurs when the skier crashes. Since the code was decrementing the `skierDirection` in the event handler and not doing a bounds check, the `skierDirection` 
  went into negative values for which no assetImage is defined.
+ Pause, Resume and Resetting of the game.
+ Implements Jumps(jumps are chosen randomly);
+ Adds units tests using the `jasmine` library. Tests are separated into their own files suffix with `-test.js`;
+ Implements a scoring system based solely on jumps. Each obstacle has a associated score and a jump over that obstacle merits the score.
+ Implements storing of previous score to localStorage. During each game, the `best score so far` is displayed.
+ Game is implemented as free skiing to a distance of `3000ft` after which the best score is stored.
+ Display of time spent during each game. Though not used now, It could be used in future for other game types.
+ Source code on [Gitlab](https://gitlab.com/soundfever/sels-ski-free)
+ Deployment to heroku: 
  1. [Game](https://sels-ski-free.herokuapp.com)
  1. [Tests](https://sels-ski-free.herokuapp.com/tests.html)

### What was not done
1. Feeding the rhino
2. Skier speed as game progresses;

### What could be improved
1. Specific key actions to jump types
2. Extra tests cases for the game manager class.
3. Extra scores for left or right move near an obstacle.
