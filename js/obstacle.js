function Obstacle(gameBoard, x, y, type, skier) {
  this.gameBoard = gameBoard;
  this.x = x;
  this.y = y;
  this.type = type;
  this.skier = skier;
}

Obstacle.prototype.setup = function () {}

Obstacle.prototype.draw = function (ctx) {
  var obstacleImage = this.gameBoard.assetStore.getAsset([this.type]);
  var x = this.x - this.skier.skierMapX - obstacleImage.width / 2;
  var y = this.y - this.skier.skierMapY - obstacleImage.height / 2;
  var twiceOffset = (2 * this.gameBoard.obstacleOffset);

  if(x < -twiceOffset || x > this.gameBoard.gameWidth + this.gameBoard.obstacleOffset || y < -twiceOffset || y > this.gameBoard.gameHeight + this.gameBoard.obstacleOffset) {
    return;
  }

  ctx.drawImage(obstacleImage, x, y, obstacleImage.width, obstacleImage.height);
}

Obstacle.prototype.update = function () {};

Obstacle.prototype.boundingBox = function(){
  var obstacleImage = this.gameBoard.assetStore.getAsset(this.type);
  var obstacleRect = {
    left: this.x,
    right: this.x + obstacleImage.width,
    top: this.y + obstacleImage.height - 5,
    bottom: this.y + obstacleImage.height
  };

  return obstacleRect;
}
