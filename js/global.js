var SkierState = {
  CRASHED: 'CRASHED',
  MOVING_LEFT: 'MOVING_LEFT',
  MOVING_LEFT_DOWN: 'MOVING_LEFT_DOWN',
  MOVING_RIGHT: 'MOVING_RIGHT',
  MOVING_RIGHT_DOWN: 'MOVING_RIGHT_DOWN',
  MOVING_DOWN: 'MOVING_DOWN',
  JUMPING: 'JUMPING',
};

var SkierStateImages = {
  CRASHED: 'skierCrash',
  MOVING_LEFT: 'skierLeft',
  MOVING_LEFT_DOWN: 'skierLeftDown',
  MOVING_RIGHT: 'skierRight',
  MOVING_RIGHT_DOWN: 'skierRightDown',
  MOVING_DOWN: 'skierDown',
  JUMP_1: 'skierJump1',
  JUMP_2: 'skierJump2',
  JUMP_3: 'skierJump3',
  JUMP_4: 'skierJump4',
  JUMP_5: 'skierJump5',
};


var ObstacleTypes = {
  ROCK_1: 'rock1',
  ROCK_2: 'rock2',
  TREE: 'tree',
  TREE_CLUSTER:'treeCluster'
};

var ScoreTable = {};
ScoreTable[ObstacleTypes.ROCK_1] = 2;
ScoreTable[ObstacleTypes.ROCK_2] = 3;
ScoreTable[ObstacleTypes.TREE] = 5;
ScoreTable[ObstacleTypes.TREE_CLUSTER] = 7;

