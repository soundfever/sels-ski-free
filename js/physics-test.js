describe('Physics', function() {
  describe('#intersectRect', function() {
    it('intersects', function() {
      var rect1 = { left: 50, top: 50, right: 100, bottom: 100 };
      var rect2 = { left: 75, top: 75, right: 120, bottom: 120 };
      expect(Physics.intersectRect(rect1, rect2)).toBe(true);
    });

    it('does not intersect', function() {
      var rect1 = { left: 50, top: 50, right: 100, bottom: 100 };
      var rect2 = { left: 120, top: 120, right: 220, bottom: 200 };
      expect(Physics.intersectRect(rect1, rect2)).toBe(false);
    });
  });

  describe('#conversion', function(){
    it('#feetToPixel', function() {
      expect(Physics.feetToPixel(30)).toEqual(300);
    });

    it('#feetToPixel', function() {
      expect(Physics.pixelToFeet(300)).toEqual(30);
    });
  });
});

