var CommandTexts = {
  NOT_STARTED: "Hit Enter to begin",
  PAUSED: "Hit Enter to resume"
};

function InstructionScreen(gameBoard) {
  this.gameBoard = gameBoard;
  this.element = null;
  this.width = 200;
  this.height = 90;

  this.titleEl = null;
  this.commandTextEl = null;
}

InstructionScreen.prototype.setup = function () {
  this.element = $('#instruction');
  this.title = this.element.find('#title');
  this.commandTextEl = this.element.find('#cmdText');

  var properties = Ui.centerByWidth(this.width, this.height, this.gameBoard.gameWidth)
  this.element.css(properties);
};

InstructionScreen.prototype.show = function () {
  this.element.show();
}

InstructionScreen.prototype.hide = function () {
  this.element.hide();
}

InstructionScreen.prototype.draw = function () {
  if(this.gameBoard.hasNotStarted() || this.gameBoard.isPaused()) {
    this.show();
  } else {
    this.hide();
  }

  if(this.gameBoard.isPaused()) {
    this.commandTextEl.text(CommandTexts.PAUSED);
  }

  if(this.gameBoard.hasNotStarted()) {
    this.commandTextEl.text(CommandTexts.NOT_STARTED);
  }
};

