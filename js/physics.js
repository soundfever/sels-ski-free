//assumption every 10 pixels is 1 meter in our game
var ConversionRate = 10;

var Physics = {
  pixelToFeet: function(pixelPosition){
    return Number((pixelPosition / ConversionRate).toFixed(0));
  },

  feetToPixel : function(feet) {
    return Number(feet * ConversionRate);
  },

  intersectRect : function(r1, r2) {
    return !(r2.left > r1.right ||
      r2.right < r1.left ||
      r2.top > r1.bottom ||
      r2.bottom < r1.top);
  }
};

