describe("Skier", function() {
  var game;
  var player;

  beforeEach(function(){
    var mockAssetStore = {
      loadedAssets: {
        "skierRight": { width: 100, height: 100 }
      },
      getAsset: function(type) {
        //mock function assumes only one type of image for all the different states;
        return this.loadedAssets["skierRight"];
      }
    };

    game = new GameManager(1000, 900, new Storage(), mockAssetStore);
    game.update = function() {};
    game.setup();
    player = new Skier(game, SkierState.MOVING_RIGHT);
    player.setup();
  });

  describe("#speed", function() {
    it("returns 0 speed when game has not started", function () {
      expect(player.speed()).toEqual(0);
    });

    it("returns a value greater than 0 when game has started", function () {
      game.play();
      expect(player.speed()).toBeGreaterThan(0);
    });

    it("returns 0 when game has stopped", function () {
      game.play();
      game.stop();
      expect(player.speed()).toEqual(0);
    });
  });

  describe("#boundingBox", function() {
    it("returns a valid bounding box", function() {

      var rect1 = { left: 500, right: 600, top: 545, bottom: 550 };
      expect(player.boundingBox()).toEqual(rect1)
    });

    it("returns a bounding box after moving", function() {
      var player = new Skier(game, SkierState.MOVING_DOWN);
      player.setup();
      player.moveToRight();

      var rect1 = { left: 500, right: 600, top: 545, bottom: 550 };
      expect(player.boundingBox()).toEqual(rect1)
    });

  });

  describe("states and movement", function () {
    describe("states", function () {
      it("returns crashed state",function(){
        player.crash();

        expect(player.hasCrashed()).toBe(true);
      });

      it("returns moving right state",function(){
        player.moveDown();
        player.moveToLeft();
        //we need to simulate the constant press on the the right arrow key
        _.times(SkierAnimationSequence.length, function() { player.moveToRight()});

        expect(player.isMovingRight()).toBe(true);
      });

      it("returns moving left state",function(){
        player.moveDown();
        _.times(SkierAnimationSequence.length, function() { player.moveToLeft()});

        expect(player.isMovingLeft()).toBe(true);
      });

      it("returns moving down state", function() {
        player.moveToLeft();
        player.moveDown();

        expect(player.isMovingDown()).toBe(true);
      });

      it("returns jump state",function(){
        player.moveDown();
        player.jump();

        expect(player.isJumping()).toBe(true);
      });
    });

    describe("movements", function (){
      it("#KeepLeft: descreases the X position", function () {
        var prevSkierX = player.skierMapX;
        player.keepLeft();

        expect(prevSkierX).toBeGreaterThan(player.skierMapX);
      });

      it("#KeepRight: increases the X position", function () {
        var prevSkierX = player.skierMapX;
        player.keepRight();

        expect(player.skierMapX).toBeGreaterThan(prevSkierX);
      });

      it("#KeepUp: descreases Y position", function () {
        var prevSkierY = player.skierMapY;
        player.keepUp();

        expect(prevSkierY).toBeGreaterThan(player.skierMapY);
      });

      it("#KeepDown: increases Y position", function () {
        var prevSkierY = player.skierMapY;
        player.keepDown();

        expect(player.skierMapY).toBeGreaterThan(prevSkierY);
      });

      it("#KeepLeftDown: decreases X position, increases Y position", function () {
        var prevSkierY = player.skierMapY;
        var prevSkierX = player.skierMapX;
        player.keepLeftDown();

        expect(prevSkierX).toBeGreaterThan(player.skierMapX);
        expect(player.skierMapY).toBeGreaterThan(prevSkierY);
      });

      it("#KeepRightDown: increases X and Y positions", function () {
        var prevSkierY = player.skierMapY;
        var prevSkierX = player.skierMapX;
        player.keepRightDown();

        expect(player.skierMapY).toBeGreaterThan(prevSkierY);
        expect(player.skierMapY).toBeGreaterThan(prevSkierY);
      });
    });
  });
});

