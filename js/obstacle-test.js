describe('Obstacle', function() {
  var game;
  var skier;
  beforeEach(function(){
    var empty = function (ctx) {}
    Obstacle.prototype.draw = empty;

    var mockAssetStore = {
      loadedAssets: {
        'rock1': { width: 100, height: 100 }
      },
      getAsset: function(type) {
        return  this.loadedAssets['rock1'];
      }
    };

    game = new GameManager(1000, 900, new Storage(), mockAssetStore);
    game.setup();
    skier = new Skier(game, SkierState.MOVING_RIGHT);
  });

  describe('#boundingBox', function() {
    it('returns a valid bounding box', function() {
      var obstacle = new Obstacle(game, 100, 100, 'rock1', skier);
      var rect1 = { left: 100, right: 200, top: 195, bottom: 200 };
      expect(obstacle.boundingBox()).toEqual(rect1)
    });
  });
});

