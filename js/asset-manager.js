var AssetManager = {
  assets: {
    'skierCrash' : 'img/skier_crash.png',
    'skierLeft' : 'img/skier_left.png',
    'skierLeftDown' : 'img/skier_left_down.png',
    'skierDown' : 'img/skier_down.png',
    'skierRightDown' : 'img/skier_right_down.png',
    'skierRight' : 'img/skier_right.png',
    'tree' : 'img/tree_1.png',
    'treeCluster' : 'img/tree_cluster.png',
    'rock1' : 'img/rock_1.png',
    'rock2' : 'img/rock_2.png',
    'jumpRamp': 'img/jump_ramp.png',
    'skierJump1': 'img/skier_jump_1.png',
    'skierJump2': 'img/skier_jump_2.png',
    'skierJump3': 'img/skier_jump_3.png',
    'skierJump4': 'img/skier_jump_4.png',
    'skierJump5': 'img/skier_jump_5.png'
  },

  loadedAssets: [],

  getAsset: function (assetName) {
    return this.loadedAssets[assetName];
  },

  loadAssets: function() {
    var assetPromises = [];
    var self = this;

    _.each(this.assets, function(asset, assetName) {
      var assetImage = new Image();
      var assetDeferred = new $.Deferred();

      assetImage.onload = function() {
        assetImage.width /= 2;
        assetImage.height /= 2;

        self.loadedAssets[assetName] = assetImage;
        assetDeferred.resolve();
      };
      assetImage.src = asset;

      assetPromises.push(assetDeferred.promise());
    });

    return $.when.apply($, assetPromises);
  }
};
