var SkierAnimationSequence = [
  SkierState.MOVING_LEFT, 
  SkierState.MOVING_LEFT_DOWN, 
  SkierState.MOVING_DOWN, 
  SkierState.MOVING_RIGHT_DOWN, 
  SkierState.MOVING_RIGHT,
];

function Skier(gameBoard, state) {
  this.originalState = state;
  this.state = state;
  this.gameBoard = gameBoard;

  this.skierMapX;
  this.skierMapY;
  this.skierSpeed;
  this.jumpType = null;
  this.numberOfJumpTypes= 5;
  this.speedRate = 1.4142;

  this.jumpDurationMin = 400;
  this.jumpDurationMax = 500;
}

Skier.prototype.setup = function () {
  this.skierMapX = 0;
  this.skierMapY = 0;
  this.skierSpeed = 8;
  this.jumpType = null;
  this.state = this.originalState;
};

Skier.prototype.draw = function (ctx) {
  var skierAssetName = this.getSkierAssetName();
  var skierImage = this.gameBoard.assetStore.getAsset(skierAssetName);
  var x = (this.gameBoard.gameWidth - skierImage.width) / 2;
  var y = (this.gameBoard.gameHeight - skierImage.height) / 2;
  ctx.drawImage(skierImage, x, y, skierImage.width, skierImage.height);
};

Skier.prototype.getSkierAssetName = function() {
  if(this.state === SkierState.JUMPING) {
    return SkierStateImages[this.jumpType];
  }
  return SkierStateImages[SkierState[this.state]];
};

Skier.prototype.speed = function() {
  if(this.gameBoard.hasNotStarted()) {
    return 0;
  }
  return this.skierSpeed;
};

Skier.prototype.keepLeft = function(){
  this.skierMapX -= this.skierSpeed;
};

Skier.prototype.keepRight = function(){
  this.skierMapX += this.skierSpeed;
};

Skier.prototype.keepUp =  function () {
  this.skierMapY -= this.skierSpeed;
};

Skier.prototype.keepDown =  function () {
  this.skierMapY += this.skierSpeed;
};

Skier.prototype.keepLeftDown =  function () {
  this.skierMapX -= Math.round(this.skierSpeed / this.speedRate);
  this.skierMapY += Math.round(this.skierSpeed / this.speedRate);
};

Skier.prototype.keepRightDown =  function () {
  this.skierMapX += this.skierSpeed / this.speedRate;
  this.skierMapY += this.skierSpeed / this.speedRate;
};

Skier.prototype.moveToLeft = function(){
  var self = this;
  var current = _.findIndex(SkierAnimationSequence, function(x){ return x === self.state});
  var desired = _.findIndex(SkierAnimationSequence, function(x){ return x === SkierState.MOVING_LEFT});

  if(current !== desired) {
    var newIndex = (current  - 1);
    newIndex = newIndex > -1 ? newIndex : 0;
    this.state = SkierAnimationSequence[newIndex];
  }
};

Skier.prototype.moveToRight = function(){
  var self = this;
  var current = _.findIndex(SkierAnimationSequence, function(x) { return x === self.state; });
  var desired = _.findIndex(SkierAnimationSequence, function(x) { return SkierState.MOVING_RIGHT === x; });
  if(current !== desired) {
    var animationSequenceLength = SkierAnimationSequence.length;
    var newIndex = (current  + 1);
    newIndex = newIndex < animationSequenceLength ? newIndex : (animationSequenceLength - 1);
    self.state =  SkierAnimationSequence[newIndex];
  }
};

Skier.prototype.moveDown = function(){
  this.state = SkierState.MOVING_DOWN;
};

Skier.prototype.crash = function(){
  this.state = SkierState.CRASHED;
};

Skier.prototype.move = function(){
  switch(this.state) {
    case SkierState.MOVING_LEFT_DOWN:
      this.keepLeftDown();
      break;

    case SkierState.MOVING_DOWN:
      this.keepDown();
      break;

    case SkierState.MOVING_RIGHT_DOWN:
      this.keepRightDown();
      break;
  }

  if(this.isJumping()) {
    this.skierMapY += this.skierSpeed;
  }
};

Skier.prototype.update = function () {
  this.move();
  if(this.isJumping()) {
    var timeElapsed = (new Date()).getTime()
    var jumpDuration = _.random(this.jumpDurationMin, this.jumpDurationMax);
    if((timeElapsed - this.jumpStartTime) >= jumpDuration) {
      this.moveDown();
    }
  }
};

Skier.prototype.boundingBox = function(){
  var skierAssetName = this.getSkierAssetName();
  var skierImage = this.gameBoard.assetStore.getAsset(skierAssetName);
  return {
    left: this.skierMapX + this.gameBoard.gameWidth / 2,
    right: this.skierMapX + skierImage.width + this.gameBoard.gameWidth / 2,
    top: this.skierMapY + skierImage.height - 5 + this.gameBoard.gameHeight / 2,
    bottom: this.skierMapY + skierImage.height + this.gameBoard.gameHeight / 2
  };
};

Skier.prototype.isMovingDown = function (){
  return (
    this.state === SkierState.MOVING_LEFT_DOWN  ||
    this.state === SkierState.MOVING_RIGHT_DOWN ||
    this.state === SkierState.MOVING_DOWN 
  );
};

Skier.prototype.isMovingLeft = function () {
  return this.state === SkierState.MOVING_LEFT;
};

Skier.prototype.isMovingRight = function () {
  return this.state === SkierState.MOVING_RIGHT;
};

Skier.prototype.isMovingUp = function () {
  return this.state === SkierState.MOVING_UP;
};

Skier.prototype.isJumping = function() {
  return this.state === SkierState.JUMPING;
};

Skier.prototype.hasCrashed = function() {
  return this.state === SkierState.CRASHED;
};

Skier.prototype.jump = function () {
  if(!this.isJumping()) {
    var jumpStateSuffix = _.random(1,this.numberOfJumpTypes);
    this.jumpType = 'JUMP_' + jumpStateSuffix;
    this.state = SkierState.JUMPING;
    this.jumpStartTime = (new Date()).getTime(); 
  }
};
