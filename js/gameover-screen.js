function GameOverScreen(gameBoard) {
  this.gameBoard = gameBoard;
  this.elemenent = null;
  this.scoreEl;
  this.score;
  this.width = 200;
  this.height = 50;
}

GameOverScreen.prototype.setup = function () {
  this.element = $('#game-over');
  this.scoreEl = this.element.find('#score');
  var properties = Ui.centerByWidth(this.width, this.height, this.gameBoard.gameWidth)
  this.element.css(properties);
}

GameOverScreen.prototype.draw = function () {
  if(this.gameBoard.isGameOver()) {
    this.scoreEl.text(this.score);
    this.element.show();
  } else {
    this.element.hide();
  }
};

GameOverScreen.prototype.updateScore = function(score) {
  this.score = score;
};
