describe('GameManager', function() {

  var game;
  beforeEach(function(){

    var mockAssetStore = {
      loadedAssets: {
        'skierRight': { width: 100, height: 100 }
      },
      getAsset: function(type) {
        //mock function assumes only one type of image for all the different states;
        return this.loadedAssets['skierRight'];
      }
    };

    game = new GameManager(1000, 900, new Storage(), mockAssetStore);
    var player = new Skier(game, SkierState.MOVING_RIGHT);
    game.addPlayer(player);
    game.setup();
    game.draw = function() {};
    game.setup();
  });

  describe('#handleScreenControls', function(){
    it('pauses the game', function() {
      game.play();
      expect(game.isPlaying()).toBe(true);
      game.handleScreenControls(GameKeys.KEY_ENTER);
      expect(game.isPaused()).toBe(true);
    });

    it('resumes the game after a pause', function() {
      game.play();
      expect(game.isPlaying()).toBe(true);
      game.handleScreenControls(GameKeys.KEY_ENTER);
      expect(game.isPaused()).toBe(true);
      game.handleScreenControls(GameKeys.KEY_ENTER);
      expect(game.isPlaying()).toBe(true);
    });

    it('resets the game', function() {
      game.play();
      expect(game.isPlaying()).toBe(true);
      game.handleScreenControls(GameKeys.KEY_R);
      expect(game.hasNotStarted()).toBe(true);
    });
  });

  describe('states', function() {
    describe('methods', function () {
      it('plays', function (){
        game.play();
        expect(game.isPlaying()).toBe(true);
      });

      it('stops/not started', function (){
        game.stop();
        expect(game.hasNotStarted()).toBe(true);
      });

      it('pauses', function (){
        game.pause();
        expect(game.isPaused()).toBe(true);
      });
    });

    describe('#checkdistance', function(){
      it('returns game over when game distance exceeded', function(){
        game.play();
        expect(game.isPlaying()).toBe(true);
        game.player.skierMapY = Physics.feetToPixel(game.maxGameDistance + 10);
        game.update();
        game.update();
        expect(game.isGameOver()).toBe(true);
      });
    });

    describe('finite state machine', function () {
      it('returns not started for a new Game', function () {
        expect(game.hasNotStarted()).toBe(true);
      });

      describe('handleGameState', function() {
        it('moves from not started to play', function(){
          expect(game.hasNotStarted()).toBe(true);
          game.handleGameState();
          expect(game.isPlaying()).toBe(true);
        });

        it('moves from play to pause', function(){
          game.play();
          expect(game.isPlaying()).toBe(true);
          game.handleGameState();
          expect(game.isPaused()).toBe(true);
        });

        it('moves from pause to play', function(){
          game.pause();
          expect(game.isPaused()).toBe(true);
          game.handleGameState();
          expect(game.isPlaying()).toBe(true);
        });

        it('moves from game over to not started', function(){
          game.play();
          expect(game.isPlaying()).toBe(true);
          game.gameOver();
          game.handleGameState();
          expect(game.hasNotStarted()).toBe(true);
        });

        it('moves from play to not started', function(){
          game.play();
          expect(game.isPlaying()).toBe(true);
          game.stop();
          expect(game.hasNotStarted()).toBe(true);
        });
      });
    });
  });
});
