var GameStates = {
  NOT_STARTED:'NOT_STARTED',
  PLAYING: 'PLAYING',
  PAUSED: 'PAUSED',
  GAME_OVER: 'GAME_OVER'
};

var GameKeys = {
  KEY_ENTER: 13,
  KEY_LEFT: 37,
  KEY_RIGHT: 39,
  KEY_UP: 38,
  KEY_DOWN: 40,
  KEY_SPACEBAR: 32,
  KEY_J: 74,
  KEY_R: 82
};

function GameManager(gameWidth, gameHeight, storage, assetManager) {
  var self = this;
  this.gameWidth = gameWidth;
  this.gameStore = storage;
  this.assetStore = assetManager;

  this.gameHeight = gameHeight;
  this.gameReady = false;
  this.player = null;
  this.obstacles = [];
  this.scoreboard = null;
  this.instruction = null;
  this.id;

  this.obstacleOffset = 50;
  this.maxGameDistance = 3000;
  this.gameRecorded = false;
  this.obstacleTypes = _.values(ObstacleTypes);

  this.reset = function () {

    this.id = (new Date).getTime();
    this.state = GameStates.NOT_STARTED;
    this.obstacles = [];

    if(this.player) {
      this.player.setup();
    }

    this.placeInitialObstacles();

    this.gameRecorded = false;

    this.scoreBoard = new ScoreBoard(this);
    this.scoreBoard.setup();
    this.scoreBoard.setBestScore(this.gameStore.getBestScore());

    this.instructionScreen = new InstructionScreen(this);
    this.instructionScreen.setup();

    this.gameOverScreen = new GameOverScreen(self);
    this.gameOverScreen.setup();
  };

  this.drawEntities = function(){
    _.each(self.obstacles, function (entity) {
      entity.draw(self.ctx);
    });
    this.player.draw(self.ctx);
  };

  this.placeInitialObstacles = function() {
    var numberObstacles = Math.ceil(_.random(5, 7) * (this.gameWidth / 800) * (this.gameHeight / 500));

    var minX = -this.obstacleOffset;
    var maxX = this.gameWidth + this.obstacleOffset;
    var minY = this.gameHeight / 2 + 100;
    var maxY = this.gameHeight + this.obstacleOffset;

    for(var i = 0; i < numberObstacles; i++) {
      self.placeRandomObstacle(minX, maxX, minY, maxY);
    }

    self.obstacles = _.sortBy(this.obstacles, function(obstacle) {
      var obstacleImage = self.assetStore.getAsset(obstacle.type);
      return obstacle.y + obstacleImage.height;
    });
  };

  this.placeRandomObstacle = function(minX, maxX, minY, maxY) {
    var obstacleIndex = _.random(0, self.obstacleTypes.length - 1);

    var position = self.calculateOpenPosition(minX, maxX, minY, maxY);

    //todo change this to obstacle class
    self.obstacles.push(
      new Obstacle(
        self,
        position.x,
        position.y,
        self.obstacleTypes[obstacleIndex],
        self.player
      )
    );
  };

  this.calculateOpenPosition = function(minX, maxX, minY, maxY) {
    var x = _.random(minX, maxX);
    var y = _.random(minY, maxY);

    var foundCollision = _.find(self.obstacles, function(obstacle) {
      return x > (obstacle.x - self.obstacleOffset) && x < (obstacle.x + self.obstacleOffset) && y > (obstacle.y - self.obstacleOffset) && y < (obstacle.y + self.obstacleOffset);
    });

    if(foundCollision) {
      return this.calculateOpenPosition(minX, maxX, minY, maxY);
    }
    else {
      return {
        x: x,
        y: y
      }
    }
  };

  this.placeNewObstacle = function(state) {
    var chance = 8;
    var shouldPlaceObstacle = _.random(1, chance);
    if(shouldPlaceObstacle !== chance) {
      return;
    }

    var leftEdge = this.player.skierMapX;
    var rightEdge = this.player.skierMapX + this.gameWidth;
    var topEdge = this.player.skierMapY;
    var bottomEdge = this.player.skierMapY + this.gameHeight;

    switch(state) {
      case SkierState.MOVING_LEFT: // left
        this.placeRandomObstacle(leftEdge - this.obstacleOffset, leftEdge, topEdge, bottomEdge);
        break;
      case SkierState.MOVING_LEFT_DOWN: // left down
        this.placeRandomObstacle(leftEdge - this.obstacleOffset, leftEdge, topEdge, bottomEdge);
        this.placeRandomObstacle(leftEdge, rightEdge, bottomEdge, bottomEdge + this.obstacleOffset);
        break;
      case SkierState.MOVING_DOWN: // down
        this.placeRandomObstacle(leftEdge, rightEdge, bottomEdge, bottomEdge + this.obstacleOffset);
        break;
      case SkierState.MOVING_RIGHT_DOWN: // right down
        this.placeRandomObstacle(rightEdge, rightEdge + this.obstacleOffset, topEdge, bottomEdge);
        this.placeRandomObstacle(leftEdge, rightEdge, bottomEdge, bottomEdge + this.obstacleOffset);
        break;
      case SkierState.MOVING_RIGHT: // right
        this.placeRandomObstacle(rightEdge, rightEdge + this.obstacleOffset, topEdge, bottomEdge);
        break;
      case SkierState.MOVING_UP: // up
        this.placeRandomObstacle(leftEdge, rightEdge, topEdge - this.obstacleOffset, topEdge);
        break;
      case SkierState.JUMPING: // jumping
        this.placeRandomObstacle(rightEdge, rightEdge + this.obstacleOffset, topEdge, bottomEdge);
        this.placeRandomObstacle(leftEdge, rightEdge, bottomEdge, bottomEdge + this.obstacleOffset);
        break;
    }
  };

  this.play = function () {
    this.state = GameStates.PLAYING;
  };

  this.pause = function () {
    this.state = GameStates.PAUSED;
  };

  this.stop = function () {
    this.state = GameStates.NOT_STARTED;
  };

  this.gameOver = function () {
    this.state = GameStates.GAME_OVER;
  };

  this.handleGameState = function () {
    switch(this.state) {
      case GameStates.NOT_STARTED:
        this.play();
        break;
      case GameStates.PLAYING:
        this.pause();
        break;
      case GameStates.PAUSED:
        this.play();
        break;
      case GameStates.GAME_OVER:
        this.reset();
        this.stop();
        break;
    }
  };

  this.handlePlayerMovement = function (key) {
    if(this.isPlaying()){
      switch(key) {
        case GameKeys.KEY_LEFT:
          if(this.player.isMovingLeft() ) {
            this.player.keepLeft();
            this.placeNewObstacle(this.player.state);
          }
          else {
            this.player.moveToLeft();
          }
          break;
        case GameKeys.KEY_RIGHT:
          if(this.player.isMovingRight()) {
            this.player.keepRight();
            this.placeNewObstacle(this.player.state);
          }
          else {
            this.player.moveToRight();
          }
          break;
        case GameKeys.KEY_UP:
          if(this.player.isMovingLeft() || this.player.isMovingRight()){
            this.player.keepUp();
            this.placeNewObstacle(this.player.state);
          }
          break;
        case GameKeys.KEY_DOWN: // down
          this.player.moveDown();
          break;
        case GameKeys.KEY_SPACEBAR: // for jump
          this.player.jump();
          this.placeNewObstacle(this.player.state);
          break;
      }
    }
  };

  this.handleScreenControls = function(key) {
    switch(key){
      case GameKeys.KEY_ENTER: //start/pausing game
        this.handleGameState();
        break;
      case GameKeys.KEY_R: // for reset game
        this.reset();
        break;
    }
  };

  this.setupKeyhandler = function() {
    var self = this;
    $(window).keydown(function(event) {
      self.handlePlayerMovement(event.which);
      self.handleScreenControls(event.which);
      event.preventDefault();
    });
  };

  this.clearCanvas= function (){
    this.ctx.clearRect(0, 0, this.gameWidth, this.gameHeight);
  }

  this.checkIfSkierHitObstacle = function() {

    var skierRect = self.player.boundingBox();

    var collision = _.find(this.obstacles, function(obstacle) {
      var obstacleRect = obstacle.boundingBox();

      return Physics.intersectRect(skierRect, obstacleRect);
    });

    if(collision) {
      if(this.player.isJumping()){ //player cannot jump over a tree cluster
        this.scoreBoard.addScore(ScoreTable[collision.type]);
      } else {
        this.player.crash();
      }
    }
  };

  this.checkDistance = function() {
    if(this.scoreBoard.distance >= this.maxGameDistance) {
      this.gameOver();
    }
  };

  this.recordScore = function() {
    if(!this.gameRecorded) {
      var score = this.scoreBoard.score;
      var gameData = {
        id: this.id,
        score: score,
        distance: this.scoreBoard.distance,
        duration: this.scoreBoard.duration
      };
      this.gameStore.commit(gameData);
      this.gameOverScreen.updateScore(score);
      this.scoreBoard.setBestScore(this.gameStore.getBestScore());
      this.gameRecorded = true;
    }
  };
};

//setup of screens and loading of assets
GameManager.prototype.setup = function () {
  var self = this;

  var canvas = $('<canvas></canvas>')
    .attr('width', this.gameWidth * window.devicePixelRatio)
    .attr('height', this.gameHeight * window.devicePixelRatio)
    .css({
      width: this.gameWidth + 'px',
      height: this.gameHeight + 'px'
    });
  $('body').append(canvas);
  this.ctx = canvas[0].getContext('2d');
  this.reset();
  self.placeInitialObstacles();
  self.setupKeyhandler();
};

GameManager.prototype.hasNotStarted = function () {
  return this.state === GameStates.NOT_STARTED;
};

GameManager.prototype.isPlaying = function () {
  return this.state === GameStates.PLAYING;
};

GameManager.prototype.isPaused = function () {
  return this.state === GameStates.PAUSED;
};

GameManager.prototype.isGameOver = function () {
  return this.state === GameStates.GAME_OVER;
};

GameManager.prototype.draw = function () {
  this.ctx.save();

  // Retina support
  this.ctx.scale(window.devicePixelRatio, window.devicePixelRatio);
  this.clearCanvas();

  //draw game Objects
  this.drawEntities();

  //draw ui
  this.scoreBoard.draw();
  this.instructionScreen.draw();
  this.gameOverScreen.draw();

  this.ctx.restore();
};

GameManager.prototype.update = function () {
  if(this.isPlaying()){
    this.player.update();
    if(this.player.isMovingDown()){
      this.placeNewObstacle(this.player.state);
    }
    this.checkIfSkierHitObstacle();
    this.checkDistance();
  }

  if(this.isGameOver()) {
    this.recordScore();
  }
  this.scoreBoard.update();
};

GameManager.prototype.run = function () {
  var self = this;
  var animate = function() {
    self.update();
    self.draw();
    requestAnimationFrame(animate);
  };
  animate();
};

GameManager.prototype.addPlayer = function addPlayer(skier) {
  skier.setup();
  this.player = skier;
};

$(document).ready(function () {
  if(window.__LIVE__){
    AssetManager.loadAssets().then(function () {
      var game = new GameManager(window.innerWidth, window.innerHeight, new Storage(), AssetManager);
      game.addPlayer(new Skier(game, SkierState.MOVING_RIGHT));
      game.setup();
      game.run();

    });
  };
});
