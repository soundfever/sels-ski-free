var Ui = {
  px: function (value) {
    return value + 'px';
  },

  centerByWidth: function (width, height, parentWidth) {
    return {
      width: this.px(width),
      height: this.px(height),
      left: this.px(parentWidth/2 - width/2),
      top: this.px(30)
    };
  },

  formatTime: function (milliseconds) {
    var time = new Date(milliseconds);
    var hours = _.padStart(time.getUTCHours(), 2, 0);
    var minutes = _.padStart(time.getUTCMinutes(), 2, 0);
    var seconds = _.padStart(time.getUTCSeconds(), 2, 0);
    var milliseconds = _.padStart(time.getUTCMilliseconds(), 3, 0);
    return hours + ':' + minutes + ':' + seconds + ':' + milliseconds;
  }
}
