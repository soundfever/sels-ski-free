function Storage() {
  this.table = "PlayedGames";

  this.createRecord = function (data) {
    return {
      gameId: data.id,
      score: data.score,
      distance: data.distance,
      duration: data.duration
    };
  };

  this.getGames = function() {
    var records = window.localStorage.getItem(this.table);
    if (records) {
      records = JSON.parse(records);
    } else {
      records = [];
    }
    return records;
  }

  this.append = function(data){
    var records = this.getGames();
    var record = this.createRecord(data);
    records.push(record);
    window.localStorage.setItem(this.table, JSON.stringify(records));
  }
}

Storage.prototype.commit = function(data){
  this.append(data);
};

Storage.prototype.getBestScore = function() {
  var records = this.getGames();
  var result = _.map(records, function(x) {
    return x.score;
  });

  return _.max(result) ||  0;
};


