function ScoreBoard(gameBoard) {
  this.gameBoard = gameBoard;
  this.distance;
  this.speed;
  this.score;
  this.lastTickTime;
  this.pausedTime;
  this.duration;
  this.pausedDuration;
  this.bestScore;

  this.timeNow = function() {
    return (new Date).getTime();
  };

  this.reset = function() {
    this.distance = 0;
    this.speed = 0;
    this.score = 0;
    this.lastTickTime = 0;
    this.pausedTime = 0;
    this.duration = 0;
    this.pausedDuration = 0;
    this.bestScore = 0;
  };

  this.updateDistance = function(distance) {
    this.distance = distance;
  };

  this.updateDuration = function () {
    if(this.gameBoard.isPlaying()){
      if(this.lastTickTime === 0){
        this.lastTickTime = this.timeNow();
      } else {
        this.duration += (this.timeNow() - this.lastTickTime - this.pausedDuration);
        this.lastTickTime = this.timeNow();
      }
      this.pausedTime = 0;
      this.pausedDuration = 0;
    }

    //track the time for pause
    if(this.gameBoard.isPaused()) {
      if(this.pausedTime === 0) {
        this.pausedTime = this.timeNow();
      } else {
        this.pausedDuration += this.timeNow() - this.pausedTime;
        this.pausedTime = this.timeNow();
      }
    }
  };
}

ScoreBoard.prototype.setup = function () {
  var scoreBoardEl = $('#scoreboard');
  this.timeEl = scoreBoardEl.find('#time');
  this.distanceEl = scoreBoardEl.find('#distance');
  this.speedEl = scoreBoardEl.find('#speed');
  this.styleEl = scoreBoardEl.find('#style');
  this.bestScoreEl = scoreBoardEl.find('#bestScore');
  this.reset();
  scoreBoardEl.show();
};

ScoreBoard.prototype.draw = function () {
  this.timeEl.text(Ui.formatTime(this.duration));
  this.speedEl.text(this.speed + 'm/s');
  this.styleEl.text(this.score);
  this.bestScoreEl.text(this.bestScore);
  this.distanceEl.text(this.distance + 'm');
};

ScoreBoard.prototype.update = function () {
  this.updateDistance(Physics.pixelToFeet(this.gameBoard.player.skierMapY));
  this.updateSpeed(this.gameBoard.player.speed());
  this.updateDuration();
};

ScoreBoard.prototype.setBestScore = function (bestScore) {
  this.bestScore = bestScore;
};

ScoreBoard.prototype.updateSpeed = function(speed) {
  this.speed = speed;
};

ScoreBoard.prototype.addScore = function(score) {
  this.score += score;
};
